<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\StatController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WellController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth
Route::post('/login', [AuthController::class, 'login']);
Route::get('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');

// User
Route::get('/users/{user_id}', [UserController::class, 'find'])->middleware('auth:sanctum');
Route::post('/users', [UserController::class, 'create']);
Route::put('/users/{user_id}', [UserController::class, 'update'])->middleware('auth:sanctum');
Route::delete('/users/{user_id}', [UserController::class, 'delete'])->middleware('auth:sanctum');

// Well
Route::get('/users/{user_id}/wells', [WellController::class, 'getByUser'])->middleware('auth:sanctum');
Route::get('/wells/{well_id}', [WellController::class, 'find'])->middleware('auth:sanctum');
Route::post('/wells', [WellController::class, 'create'])->middleware('auth:sanctum');
Route::put('/wells/{well_id}', [WellController::class, 'update'])->middleware('auth:sanctum');
Route::delete('/wells/{well_id}', [WellController::class, 'delete'])->middleware('auth:sanctum');

// Stat
Route::get('/wells/{well_id}/stats', [StatController::class, 'getByWell'])->middleware('auth:sanctum');
Route::post('/stats', [StatController::class, 'create']);

