<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Well;
use Illuminate\Http\Request;

class WellController extends Controller
{
    public function getByUser($user_id)
    {
        $response = response(Well::where('user_id', $user_id)->get(), 200);

        if($response->content() == "[]")
        {
            $response = response('', 404);
        }

        return $response;
    }
    
    public function find($well_id)
    {
        $response = Well::where('id', $well_id)->first();
        if(!$response)
        {
            $response = response('', 404);
        }
        return $response;
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'string|required|max:255',
            'state' => 'boolean|required',
            'type' => 'string|required|max:255',
            'depth' => 'numeric|required',
            'level_min' => 'numeric|required',
            'level_min_relaunch' => 'numeric|required',
            'volume' => 'numeric|required',
            'stat_frequency' => 'string|required|max:255',
            'photo' => 'string|max:255',
            'address' => 'string|required|max:255',
            'state_presostat' => 'boolean|required'
        ]);

        $current_user = $request->user();

        $new_well = new Well;
        $new_well->name = $request['name'];
        $new_well->state = $request['state'];
        $new_well->type = $request['type'];
        $new_well->depth = $request['depth'];
        $new_well->level_min = $request['level_min'];
        $new_well->level_min_relaunch = $request['level_min_relaunch'];
        $new_well->volume = $request['volume'];
        $new_well->stat_frequency = $request['stat_frequency'];
        $new_well->photo = $request['photo'];
        $new_well->address = $request['address'];
        $new_well->state_presostat = $request['state_presostat'];
        $new_well->user()->associate(User::where('id', $current_user['id'])->first()->id);
        $new_well->save();

        return response($new_well, 201);
    }

    public function update(Request $request, $well_id)
    {
        $request->validate([
            'name' => 'string|required|max:255',
            'state' => 'boolean|required',
            'type' => 'string|required|max:255',
            'depth' => 'numeric|required',
            'level_min' => 'numeric|required',
            'level_min_relaunch' => 'numeric|required',
            'volume' => 'numeric|required',
            'stat_frequency' => 'string|required|max:255',
            'photo' => 'string|max:255',
            'address' => 'string|required|max:255',
            'state_presostat' => 'boolean|required'
        ]);

        $current_user = $request->user();

        $update_well = Well::where('id', $well_id)->first();

        if($update_well == null)
        {
            return response("Le puit n'existe pas ou n'a pas été trouvé", 404);
        }

        $update_well->name = $request['name'];
        $update_well->state = $request['state'];
        $update_well->type = $request['type'];
        $update_well->depth = $request['depth'];
        $update_well->level_min = $request['level_min'];
        $update_well->level_min_relaunch = $request['level_min_relaunch'];
        $update_well->volume = $request['volume'];
        $update_well->stat_frequency = $request['stat_frequency'];
        $update_well->photo = $request['photo'];
        $update_well->address = $request['address'];
        $update_well->state_presostat = $request['state_presostat'];
        $update_well->user()->associate(User::where('id', $current_user['id'])->first()->id);
        $update_well->save();

        return response($update_well, 200);
    }

    public function delete($well_id)
    {
        $delete_well = Well::where('id', $well_id)->first();
        if($delete_well == null)
        {
            return response("Le puit n'existe pas ou n'a pas été trouvé", 404);
        }
        $delete_well->delete();
        return response($well_id, 200);
    }    
}
