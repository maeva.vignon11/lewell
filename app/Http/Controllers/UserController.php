<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function find($user_id)
    {
        $user = User::where('id', $user_id)->first();

        if($user == null)
        {
            return response('', 404);
        }

        return response($user, 200);
    }

    public function create(Request $request)
    {
        $request->validate([
            'lastname' => 'string|required|max:255',
            'firstname' => 'string|required|max:255',
            'email' => 'email:rfc,dns',
            'password' => 'string|required|max:255',
            'photo' => 'string|nullable|max:255'
        ]);

        $new_user = new User();
        $new_user->lastname = $request['lastname'];
        $new_user->firstname = $request['firstname'];
        $new_user->email = $request['email'];
        $new_user->password = Hash::make($request['password']);
        $new_user->photo = $request['photo'];
        $new_user->save();

        return response($new_user, 201);
    }

    public function update(Request $request, $user_id)
    {
        $request->validate([
            'lastname' => 'string|required|max:255',
            'firstname' => 'string|required|max:255',
            'email' => 'email:rfc,dns',
            'password' => 'string|required|max:255',
            'photo' => 'string|nullable|max:255'
        ]);

        $update_user = User::where('id', $user_id)->first();

        if($update_user == null)
        {
            return response("L'utilisateur n'existe pas ou n'a pas été trouvé", 404);
        }

        $update_user->lastname = $request['lastname'];
        $update_user->firstname = $request['firstname'];
        $update_user->email = $request['email'];
        $update_user->password = Hash::make($request['password']);
        $update_user->photo = $request['photo'];
        $update_user->save();

        return response($update_user, 200);
    }

    public function delete(Request $request, $user_id)
    {
        $delete_user = User::where('id', $user_id)->first();
        if($delete_user == null)
        {
            return response("L'utilisateur n'existe pas ou n'a pas été trouvé", 404);
        }
        $delete_user->delete();
        return response($user_id, 200);
    }
}
