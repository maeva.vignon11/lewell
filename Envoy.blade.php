@servers(['web' => 'deployeur@4.233.83.138'])

@setup
    $repository = 'git@gitlab.com:maeva.vignon11/lewell.git';
    $releases_dir = '/var/www/lewell/releases';
    $app_dir = '/var/www/lewell';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
    migrations
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
    chmod -R 777 storage
    chmod -R 777 bootstrap/
    cp .env.example .env
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('migrations')
    echo "Play migration"
	cd {{ $app_dir }}
    cd current
    php artisan config:cache
    php artisan route:cache
    php artisan migrate:fresh
    php artisan db:seed
@endtask