<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_stat' => $this->faker->date(),
            'level_water' => $this->faker->randomFloat(2, 0, 100),
            'is_consolidated' => $this->faker->boolean(),
        ];
    }
}
