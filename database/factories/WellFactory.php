<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class WellFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'state' => $this->faker->boolean(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->randomFloat(2, 0, 100),
            'level_min' => round($this->faker->randomFloat(2, 0, 5), 2),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];
    }
}
