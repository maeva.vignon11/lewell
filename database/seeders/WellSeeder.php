<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Well;
use Illuminate\Database\Seeder;

class WellSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $well = Well::factory()->make();
        $well->user()->associate(User::first());
        $well->save();
    }
}
