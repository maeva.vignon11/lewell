<?php

namespace Database\Seeders;

use App\Models\Stat;
use App\Models\Well;
use Illuminate\Database\Seeder;

class StatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stat = Stat::factory()->make();
        $stat->well()->associate(Well::first());
        $stat->save();
    }
}
