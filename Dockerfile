FROM php:7.4.2-fpm-alpine3.11

#RUN apk-get update

# Install dev dependencies
RUN apk add --no-cache --virtual .build-deps \
    $PHPIZE_DEPS \
    curl-dev \
    imagemagick-dev \
    libtool \
    libxml2-dev \
    postgresql-dev \
    sqlite-dev

# Install production dependencies
RUN apk add --no-cache \
    bash \
    curl \
    g++ \
    gcc \
    git \
    imagemagick \
    libc-dev \
    libpng-dev \
    make \
    nodejs>12.22.1 \
    npm \
    yarn \
    openssh-client \
    postgresql-libs \
    rsync \
    zlib-dev \
    libzip-dev

# Install mysql
RUN apk add --no-cache \
    mysql \
    mysql-client

# Install PECL and PEAR extensions
RUN pecl install \
    imagick

#RUN apt-get clean

# Install and enable php extensions
RUN docker-php-ext-enable \
    imagick
RUN docker-php-ext-configure zip
RUN docker-php-ext-install \
    curl \
    iconv \
    pdo \
    pdo_mysql \
    pdo_pgsql \
    pdo_sqlite \
    pcntl \
    tokenizer \
    xml \
    zip \
    bcmath

# Install GD
RUN apk add --no-cache freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev && \
  docker-php-ext-configure gd \
   && \
  NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
  docker-php-ext-install -j${NPROC} gd && \
  apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev

# Install composer
#ENV COMPOSER_HOME /composer
#ENV PATH ./vendor/bin:/composer/vendor/bin:$PATH
#ENV COMPOSER_ALLOW_SUPERUSER 1
RUN curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer global require "laravel/envoy"


# Cleanup dev dependencies
#RUN apk del -f .build-deps

# Setup working directory
#WORKDIR /var/www
