<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Stat;
use App\Models\Well; 
use Illuminate\Support\Facades\Hash;

class CreateStatistiqueTest extends TestCase
{
    use WithFaker;

    // check if the authenticated user can create a new stat
    public function test_create_a_new_stat()
    {
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);

        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'date_stat' => $this->faker->date('Y-m-d'),
            'level_water' => $this->faker->randomNumber(3),
            'is_consolidated' => $this->faker->boolean,
            'well_id' => $well->id,
        ];

        $response = $this->withHeaders($headers)->postJson('/api/stats', $data);
        $response->assertStatus(201);

        $this->assertDatabaseHas('stats', [
            'date_stat' => $data['date_stat'],
            'level_water' => $data['level_water'],
            'is_consolidated' => $data['is_consolidated'],
            'well_id' => $data['well_id'],
        ]);
    }

    public function test_create_with_missing_data()
    {
        $wellIds = Well::pluck('id')->toArray();
        $user = User::factory()->create();
        $token = $user->createToken('TestToken')->plainTextToken;

        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'date_stat' => $this->faker->date('Y-m-d'),
            'is_consolidated' => $this->faker->boolean,
            'well_id' => $this->faker->randomElement($wellIds),
        ];

        $response = $this->withHeaders($headers)->postJson('/api/stats', $data);
        $response->assertStatus(422);
    }

    public function test_create_with_invalid_data() {
        $user = User::factory()->create();
        $token = $user->createToken('TestToken')->plainTextToken;

        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'date_stat' => $this->faker->word(),
            'level_water' => $this->faker->word(),
            'is_consolidated' => $this->faker->word(),
            'well_id' => $this->faker->word(),
        ];

        $response = $this->withHeaders($headers)->postJson('/api/stats', $data);
        $response->assertStatus(422);
    }

    public function test_create_with_bad_id()
    {
        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'date_stat' => $this->faker->date('Y-m-d'),
            'level_water' => $this->faker->randomNumber(3),
            'is_consolidated' => $this->faker->boolean,
            'well_id' => 999999,
        ];

        $response = $this->withHeaders($headers)->postJson('/api/stats', $data);
        $response->assertStatus(404);
    }
}
