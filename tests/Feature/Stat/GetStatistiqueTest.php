<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Well; 
use App\Models\Stat; 
use App\Models\User; 

class GetStatistiqueTest extends TestCase
{
    use WithFaker;

    public function test_get_all_stat()
    {        
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);

        $stats = Stat::factory(3)->create(['well_id' => $well->id]);
        $token = $user->createToken('auth_token')->plainTextToken;
        $bearerToken = 'Bearer ' . $token;
        $response = $this->withHeader('Authorization', $bearerToken)->getJson("/api/wells/". $well->id. "/stats");
        
        $response->assertStatus(200);  
    }

    public function test_get_by_bad_id() {
        $user = User::factory()->create();

        $token = $user->createToken('auth_token')->plainTextToken; 
        $bearerToken = 'Bearer ' . $token;

        $response = $this->withHeader('Authorization', $bearerToken)->getJson("/api/wells/". 99999999 . "/stats");
        
        $response->assertStatus(404);
    }

    public function test_get_with_missing_token()
    {        
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);

        $stats = Stat::factory(3)->create(['well_id' => $well->id]);
        $response = $this->getJson("/api/wells/". $well->id. "/stats");
        
        $response->assertStatus(401)
                ->assertJson([
                    'message' => 'Unauthenticated.',
                ]);  
    }
}