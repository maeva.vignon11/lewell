<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    public function test_update_user() {
        $user = User::factory()->create();

        $token = $user->createToken('TestToken')->plainTextToken;
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'lastname' => $this->faker->word(),
            'firstname' => $this->faker->word(),
            'email' => $this->faker->email(),
            'password' => $this->faker->password(),
            'photo' => $this->faker->word(),
        ];

        $response = $this->withHeaders($headers)->putJson('/api/users/'. $user->id, $data);
        $response->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'lastname' => $data['lastname'],
            'firstname' => $data['firstname'],
            'email' => $data['email'],
            'photo' => $data['photo'],
        ]);

        // $this->assertEquals(true, Auth::validate([$data['email'], $data['password']]));
    }

    public function test_update_user_without_token() {
        $user = User::factory()->create();

        $data = [
            'lastname' => $this->faker->word(),
            'firstname' => $this->faker->word(),
            'email' => $this->faker->email(),
            'password' => $this->faker->password(),
            'photo' => $this->faker->word(),
        ];

        $response = $this->putJson('/api/users/'. $user->id, $data);
        $response->assertStatus(401)
                ->assertJson([
                    'message' => 'Unauthenticated.',
                ]);
    }

    public function test_update_user_with_missing_data() {
        $user = User::factory()->create();

        $token = $user->createToken('TestToken')->plainTextToken;
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'firstname' => $this->faker->word(),
            'email' => $this->faker->email(),
            'password' => $this->faker->password(),
            'photo' => $this->faker->word(),
        ];

        $response = $this->withHeaders($headers)->putJson('/api/users/'. $user->id, $data);
        $response->assertStatus(422);        
    }

    public function test_update_user_with_invalid_data() {
        $user = User::factory()->create();

        $token = $user->createToken('TestToken')->plainTextToken;
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'lastname' => $this->faker->randomFloat(2, 0, 10),
            'firstname' => $this->faker->randomFloat(2, 0, 10),
            'email' => $this->faker->email(),
            'password' => $this->faker->randomFloat(2, 0, 10),
            'photo' => $this->faker->randomFloat(2, 0, 10),
        ];

        $response = $this->withHeaders($headers)->putJson('/api/users/'. $user->id, $data);
        $response->assertStatus(422);
    }

    public function test_update_user_with_missing_user() {
        $user = User::factory()->create();

        $token = $user->createToken('TestToken')->plainTextToken;
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'lastname' => $this->faker->word(),
            'firstname' => $this->faker->word(),
            'email' => $this->faker->email(),
            'password' => $this->faker->password(),
            'photo' => $this->faker->word(),
        ];

        $response = $this->withHeaders($headers)->putJson('/api/users/9999', $data);
        $response->assertStatus(404);
    }
}
