<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash; 

class FindUserTest extends TestCase
{
    use WithFaker;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_get_find_user()    
    {
        $user = User::factory()->create([
            'password' => Hash::make('password'),
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;
        $bearerToken = 'Bearer ' . $token;
        $response = $this->withHeaders([
            'Authorization' => $bearerToken,
        ])->get('/api/users/' . $user->id);

        $response->assertStatus(200)
            ->assertJson([
                'id' => $user->id,
                'email' => $user->email,
            ]);
    }


    public function test_find_without_token()
    {
        $user = User::factory()->create();

        $response = $this->getJson('/api/users/' . $user->id);

        $response->assertStatus(401)
                ->assertJson([
                    'message' => 'Unauthenticated.',
                ]);
    }

    public function test_find_missing_user()
    {
        $user = User::factory()->create([
            'password' => Hash::make('password'),
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;
        $bearerToken = 'Bearer ' . $token;
        $response = $this->withHeaders([
            'Authorization' => $bearerToken,
        ])->get('/api/users/9999');

        $response->assertStatus(404);
    }
}
