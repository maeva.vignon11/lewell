<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    public function test_create_user_with_valid_data()
    {
        $userData = [
            'lastname' => $this->faker->lastName,
            'firstname' => $this->faker->firstName,
            'email' => $this->faker->unique()->safeEmail,
            'password' => 'password123',
            'photo' => 'path/to/photo.jpg',
        ];

        $response = $this->postJson('/api/users', $userData);

        $response->assertStatus(422)->assertJsonValidationErrors(['email']);
    }


    public function test_create_user_with_invalid_data()
    {
        $invalidUserData = [
            'lastname' => '', 
            'firstname' => 'John', 
            'email' => 'invalid_email', 
            'password' => 'pass', 
            'photo' => null,
        ];

        $response = $this->postJson('/api/users', $invalidUserData);

        $response->assertStatus(422) 
                ->assertJsonValidationErrors(['lastname', 'email']);
    }

    public function test_create_user_with_existing_email()
    {
        $existingUser = User::factory()->create();

        $userData = [
            'lastname' => $this->faker->lastName,
            'firstname' => $this->faker->firstName,
            'email' => $existingUser->email, // Email déjà existant
            'password' => 'password123',
            'photo' => 'path/to/photo.jpg',
        ];

        $response = $this->postJson('/api/users', $userData);

        $response->assertStatus(422)
                ->assertJsonValidationErrors(['email']);
    }

    public function test_create_user_with_invalid_data_types()
    {
        $userData = [
            'lastname' => 123, 
            'firstname' => ['John'],
            'email' => 'invalid_email',
            'password' => true, 
            'photo' => 123,
        ];

        $response = $this->postJson('/api/users', $userData);

        $response->assertStatus(422)
                ->assertJsonValidationErrors(['lastname', 'firstname', 'email', 'password', 'photo']);
    }

}
