<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Well;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class GetWellByUserTest extends TestCase
{
    use DatabaseTransactions; // Pour s'assurer que la base de données est rafraîchie avant chaque test

    // Test lorsque le token n'est pas valide
    public function testGetByUserWithInvalidToken()
    {
        $user = User::factory()->create();

        // Appeler la route avec un ID utilisateur valide
        $response = $this->getJson('/api/users/'.$user->id.'/wells');

        $response->assertStatus(401)
                ->assertJson([
                    'message' => 'Unauthenticated.',
                ]);
    }

    // Test lorsque l'utilisateur est introuvable
    public function testGetByUserWithNonexistentUser()
    {
        // Créer un ID utilisateur inexistant
        $nonExistentUserId = 999;

        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();
        $wells = Well::factory(3)->create(['user_id' => $user->id]);

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        // Appeler la route avec un ID utilisateur inexistant
        $response = $this->withHeaders($headers)->get('/api/users/'.$nonExistentUserId.'/wells');

        // Vérifier que la réponse est 404 Not Found
        $response->assertStatus(404);
    }

    // Test lorsque l'utilisateur est trouvé sans problème
    public function testGetByUserSuccessfully()
    {
        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();
        $wells = Well::factory(3)->create(['user_id' => $user->id]);

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        // Appeler la route avec l'ID utilisateur créé et le token d'authentification
        $response = $this->withHeaders($headers)->get('/api/users/'.$user->id.'/wells');

        // Vérifier que la réponse est 200 OK
        $response->assertStatus(200);

        // Vérifier que les puits renvoyés sont corrects
        // $response->assertJson($wells->toArray());
        $wells_found = $response->json();

        foreach ($wells as $well) {
            $well_found = null;

            foreach ($wells_found as $wellfound) {
                if ($wellfound['id'] == $well->id) {
                    $well_found = $wellfound;
                }
            }

            $this->assertEquals(round($well_found['depth'], 2), round($well->depth, 2));
            $this->assertEquals(round($well_found['level_min_relaunch'], 2), round($well->level_min_relaunch, 2));
            $this->assertEquals(round($well_found['level_min'], 2), round($well->level_min, 2));
            $this->assertEquals($well_found['id'], $well->id);
            $this->assertEquals($well_found['name'], $well->name);
            $this->assertEquals($well_found['state'], $well->state);
            $this->assertEquals($well_found['type'], $well->type);
            $this->assertEquals($well_found['volume'], $well->volume);
            $this->assertEquals($well_found['stat_frequency'], $well->stat_frequency);
            $this->assertEquals($well_found['photo'], $well->photo);
            $this->assertEquals($well_found['address'], $well->address);
            $this->assertEquals($well_found['state_presostat'], $well->state_presostat);
            $this->assertEquals($well_found['photo'], $well->photo);
            $this->assertEquals($well_found['user_id'], $well->user_id);
        }
    }
}
