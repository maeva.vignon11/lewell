<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Well;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateWellTest extends TestCase
{
    use DatabaseTransactions; // Pour s'assurer que la base de données est rafraîchie avant chaque test
    use WithFaker;

    public function testUpdateWell()
    {
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'name' => $this->faker->word(),
            'state' => $this->faker->boolean(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->randomFloat(2, 0, 100),
            'level_min' => round($this->faker->randomFloat(2, 0, 5), 2),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->withHeaders($headers)->putJson('/api/wells/'.$well->id, $data);
        $response->assertStatus(200);

        $well_updated = Well::where('id', $well->id)->first();

        $this->assertEquals(round($well_updated->depth, 2), round($data['depth'], 2));
        $this->assertEquals(round($well_updated->level_min_relaunch, 2), round($data['level_min_relaunch'], 2));
        $this->assertEquals(round($well_updated->level_min, 2), round($data['level_min'], 2));
        $this->assertEquals($well_updated->name, $data['name']);
        $this->assertEquals($well_updated->state, $data['state']);
        $this->assertEquals($well_updated->type, $data['type']);
        $this->assertEquals($well_updated->volume, $data['volume']);
        $this->assertEquals($well_updated->stat_frequency, $data['stat_frequency']);
        $this->assertEquals($well_updated->photo, $data['photo']);
        $this->assertEquals($well_updated->address, $data['address']);
        $this->assertEquals($well_updated->state_presostat, $data['state_presostat']);

        // $this->assertDatabaseHas('wells', [
        //     'name' => $data['name'],
        //     'state' => $data['state'],
        //     'type' => $data['type'],
        //     'depth' => $data['depth'],
        //     'level_min' => $data['level_min'],
        //     'level_min_relaunch' => $data['level_min_relaunch'],
        //     'volume' => $data['volume'],
        //     'stat_frequency' => $data['stat_frequency'],
        //     'photo' => $data['photo'],
        //     'address' => $data['address'],
        //     'state_presostat' => $data['state_presostat'],
        // ]);
    }

    public function testUpdateWellWithMissingData()
    {
        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'state' => $this->faker->boolean(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->randomFloat(2, 0, 100),
            'level_min' => round($this->faker->randomFloat(2, 0, 5), 2),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->withHeaders($headers)->putJson('/api/wells/'.$well->id, $data);
        $response->assertStatus(422);
    }

    public function testUpdateWellWithInvalidData()
    {
        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'name' => $this->faker->randomFloat(2, 0, 10),
            'state' => $this->faker->word(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->boolean(),
            'level_min' =>  $this->faker->word(),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->withHeaders($headers)->putJson('/api/wells/'.$well->id, $data);
        $response->assertStatus(422);
    }

    public function testUpdateWithMissingWell()
    {
        $user = User::factory()->create();

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'name' => $this->faker->word(),
            'state' => $this->faker->boolean(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->randomFloat(2, 0, 100),
            'level_min' => round($this->faker->randomFloat(2, 0, 5), 2),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->withHeaders($headers)->putJson('/api/wells/99999', $data);
        $response->assertStatus(404);
    }

    public function testUpdateWellWithInvalidToken()
    {
        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();
        $well = Well::factory()->create(['user_id' => $user->id]);

        $data = [
            'state' => $this->faker->boolean(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->randomFloat(2, 0, 100),
            'level_min' => round($this->faker->randomFloat(2, 0, 5), 2),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->putJson('/api/wells/'.$well->id, $data);

        $response->assertStatus(401)
                ->assertJson([
                    'message' => 'Unauthenticated.',
                ]);
    }
}
