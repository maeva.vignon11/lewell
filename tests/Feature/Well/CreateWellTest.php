<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Well;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateWellTest extends TestCase
{
    use DatabaseTransactions; // Pour s'assurer que la base de données est rafraîchie avant chaque test
    use WithFaker;

    public function testCreateWell()
    {
        $user = User::factory()->create();

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'name' => $this->faker->word(),
            'state' => $this->faker->boolean(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->randomFloat(2, 0, 100),
            'level_min' => round($this->faker->randomFloat(2, 0, 5), 2),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->withHeaders($headers)->postJson('/api/wells', $data);
        $response->assertStatus(201);

        $id_well_created = $response->json()['id'];

        $well_created = Well::where('id', $id_well_created)->first();

        $this->assertEquals(round($well_created->depth, 2), round($data['depth'], 2));
        $this->assertEquals(round($well_created->level_min_relaunch, 2), round($data['level_min_relaunch'], 2));
        $this->assertEquals(round($well_created->level_min, 2), round($data['level_min'], 2));
        $this->assertEquals($well_created->name, $data['name']);
        $this->assertEquals($well_created->state, $data['state']);
        $this->assertEquals($well_created->type, $data['type']);
        $this->assertEquals($well_created->volume, $data['volume']);
        $this->assertEquals($well_created->stat_frequency, $data['stat_frequency']);
        $this->assertEquals($well_created->photo, $data['photo']);
        $this->assertEquals($well_created->address, $data['address']);
        $this->assertEquals($well_created->state_presostat, $data['state_presostat']);
    }

    public function testCreateWellWithMissingData()
    {
        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'state' => $this->faker->boolean(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->randomFloat(2, 0, 100),
            'level_min' => round($this->faker->randomFloat(2, 0, 5), 2),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->withHeaders($headers)->postJson('/api/wells', $data);
        $response->assertStatus(422);
    }

    public function testCreateWellWithInvalidData()
    {
        // Créer un utilisateur et des puits associés
        $user = User::factory()->create();

        // Authentifier l'utilisateur et obtenir le token Sanctum
        $token = $user->createToken('TestToken')->plainTextToken;

        // Ajouter le token à la requête HTTP
        $headers = ['Authorization' => 'Bearer ' . $token];

        $data = [
            'name' => $this->faker->randomFloat(2, 0, 10),
            'state' => $this->faker->word(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->boolean(),
            'level_min' =>  $this->faker->word(),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->withHeaders($headers)->postJson('/api/wells', $data);
        $response->assertStatus(422);
    }

    public function testCreateWellWithInvalidToken()
    {
        $data = [
            'state' => $this->faker->boolean(),
            'type' =>  $this->faker->word(),
            'depth' =>  $this->faker->randomFloat(2, 0, 100),
            'level_min' => round($this->faker->randomFloat(2, 0, 5), 2),
            'level_min_relaunch' => round($this->faker->randomFloat(2, 0, 10), 2),
            'volume' =>  $this->faker->randomFloat(2, 0, 1000),
            'stat_frequency' =>  $this->faker->word(),
            'photo' =>  $this->faker->word(),
            'address' =>  $this->faker->address(),
            'state_presostat' =>  $this->faker->boolean(),
        ];

        $response = $this->postJson('/api/wells', $data);

        $response->assertStatus(401)
                ->assertJson([
                    'message' => 'Unauthenticated.',
                ]);
    }
}
